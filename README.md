## Getting started locally

#### Create a virtual environment and install requirements

```bash
sudo apt-get install python3-venv # if venv not installed

python3 -m venv myenv

source myenv/bin/activate
```

#### Install requirements

```bash
pip install -r requirements.txt
```

#### Start app locally

```bash
python app.py
```


## Deployment

- Visit the project's event page on render.com https://dashboard.render.com/web/srv-cj1ei298g3ne7u6j0msg/events

- After commiting changes to the main branch and pushing to remote, click "Deploy latest commit" under "Manual deploy"
![](/images/deploy.png)

## Endpoints

#### https://gridmatic-api.onrender.com/grid-mix

Payload:
```json
{
    "timestamp": [1684818000, 1684821600, 1684825200...],
    "carbon_free_grid_mix_mw": [17064.74166666667, 16065.574999999999, 14378.633333333331...],
    "non_carbon_free_grid_mix_mw": [25995.783333333333, 25079.50833333333, 25529.649999999998...],
}
```

---
#### https://gridmatic-api.onrender.com/timestamp

Payload:
```json
{
    "timestamp": [1684818000, 1684821600, 1684825200...],
}
```

---

#### https://gridmatic-api.onrender.com/carbon-free

Payload: (This enpoint has a 5 second delay built in)
```json
{
    "carbon_free_grid_mix_mw": [17064.74166666667, 16065.574999999999, 14378.633333333331...],
}
```

---

#### https://gridmatic-api.onrender.com/non-carbon-free

Payload: (This enpoint has a 10 second delay built in)
```json
{
    "non_carbon_free_grid_mix_mw": [25995.783333333333, 25079.50833333333, 25529.649999999998...],
}
```